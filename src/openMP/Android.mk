LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_FLAGS := -DDEBUGLEVEL=0 -O3 -fopenmp
LOCAL_LDFLAGS := -O3 -fopenmp

LOCAL_C_INCLUDES := $(NDK_PROJECT_PATH)
LOCAL_SRC_FILES := \
	../../benchit.c \
	work.c \
	kernel_main.c

LOCAL_MODULE := openmp_membw

LOCAL_STATIC_LIBRARIES := libutils

include $(BUILD_EXECUTABLE)
