LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CFLAGS := -O3
LOCAL_SRC_FILES := \
	bienvhash.c \
	bitWriter.c \
	gnuWriter.c \
	output.c \
	stringlib.c

LOCAL_MODULE := utils

include $(BUILD_STATIC_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))
