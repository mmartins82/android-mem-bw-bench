LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CFLAGS += -DAFFINITY
LOCAL_SRC_FILES := \
	properties.c \
	x86.c \
	generic.c

LOCAL_MODULE := x86

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := architecture.c

LOCAL_MODULE := cpuinfo
LOCAL_STATIC_LIBRARIES := libx86

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := hw_detect.c
LOCAL_MODULE := hw_detect

LOCAL_STATIC_LIBRARIES := libx86

include $(BUILD_EXECUTABLE)
