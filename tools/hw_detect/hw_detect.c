#include <math.h>
#include <stdio.h>
#include <unistd.h>

#include "cpu.h"

unsigned long long get_memsize(void) {
    long num_pages = sysconf(_SC_PHYS_PAGES);
    long pagesize = sysconf(_SC_PAGESIZE);

    if ((num_pages != 1) && (pagesize != -1)) {
        return (unsigned long long) num_pages * pagesize;
    }

    return -1;
}

int main(int argc, char *argv[])
{
    printf("# of CPU cores: %d\n", num_cpus());
    printf("Arch speed: %d MHz\n",
           (int) floor(get_cpu_clockrate(1, 0) / 1e6));
    printf("Total mem size: %llu B\n", get_memsize());
    printf("Recommended max mem: %d MB\n",
           (int) floor((get_memsize() * 0.85) / 1048576));
    return 0;
}
